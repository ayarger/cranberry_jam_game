﻿using UnityEngine;
using System.Collections;

public class reverse_gravity : MonoBehaviour {

	bool invertGravity = false;
	Rigidbody rb;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
		InvertGravity();
	}

	void FixedUpdate () {
		if (invertGravity) rb.AddForce(-Physics.gravity, ForceMode.Acceleration);
	}

	void InvertGravity () {
		invertGravity=!invertGravity;
		if (invertGravity) rb.useGravity=false; else GetComponent<Rigidbody>().useGravity=true;
	}
}
