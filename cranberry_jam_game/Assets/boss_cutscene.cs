﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class boss_cutscene : MonoBehaviour {

	public List<Element> ele_queue = new List<Element>();

	public GameObject blackboard;
	public GameObject ship;
	public GameObject boss;

	public AudioClip john_cena_sound;

	public Text warning_text;
	public float warning_text_sincounter = 0.0f;
	public bool warning = true;

	// Use this for initialization
	void Start () {
	
		// Initial camera.
		transform.position = new Vector3(19.8f, -65, 210.6f);
		Quaternion q = new Quaternion();
		q.eulerAngles = new Vector3(0, -44.36f, 0);
		transform.rotation = q;

		Element.addElement(ele_queue, new ElementMoveOverTime(360, new Vector3(0.52f, -67.5f, 246.5f), new Vector3(0.52f, -67.5f, 177), ship, false));
	
		// Second camera point.
		Element.addElement(ele_queue, new ElementExecuteAction(() => {
			transform.position = new Vector3(1.18f, -53.13f, 284.07f);
			Quaternion q2 = new Quaternion();
			q2.eulerAngles = new Vector3(39.61f, 180.0f, 0);
			transform.rotation = q2;
		}));

		Element.addElement(ele_queue, new ElementMoveOverTime(360, new Vector3(0.52f, -67.5f, 293.21f), new Vector3(0.52f, -67.5f, 246.5f), ship, false));
	
		Element.addElement(ele_queue, new ElementExecuteAction(() => {
			Destroy(ship);
		}));

		// Third camera point.
		Element.addElement(ele_queue, new ElementExecuteAction(() => {
			transform.position = new Vector3(19.24f, -35.6f, 265.43f);
			Quaternion q3 = new Quaternion();
			q3.eulerAngles = new Vector3(35.1f, 333.9f, 0);
			transform.rotation = q3;
			warning = false;
		}));

		Element.addElement(ele_queue, new ElementExecuteAction(() => {
			AudioSource.PlayClipAtPoint(john_cena_sound, Camera.main.transform.position);
		}));

		// Dinosaur appearance.
		Element.addElement(ele_queue, new ElementMoveOverTime(360, new Vector3(0.0f, -77.4f, 373), new Vector3(0, -111.29f, 373.0f), boss, false));

		// Fourth camera point.
		Element.addElement(ele_queue, new ElementExecuteAction(() => {
			transform.position = new Vector3(34.8f, -35.4f, 265.43f);
			Quaternion q3 = new Quaternion();
			q3.eulerAngles = new Vector3(19.5f, 333.9f, 0);
			transform.rotation = q3;
		}));
		Element.addElement(ele_queue, new ElementNoop(240));
		Element.addElement(ele_queue, new ElementLoadScene("boss"));
	}
	
	// Update is called once per frame
	void Update () {
		Element.updateElements(ele_queue);

		if(warning)
		{
			warning_text.color = new Color(warning_text.color.r, warning_text.color.g, warning_text.color.b, Mathf.Sin(warning_text_sincounter));
			warning_text_sincounter += 0.1f;
		}
		else
		{
			warning_text.color = new Color(warning_text.color.r, warning_text.color.g, warning_text.color.b, 0);
		}
	}
}
