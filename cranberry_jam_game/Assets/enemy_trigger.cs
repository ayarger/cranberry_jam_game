﻿using UnityEngine;
using System.Collections;

public class enemy_trigger : MonoBehaviour {

	public int num_enemies;
	public GameObject enemy_prefab;

	// Update is called once per frame
	void Update () {
	
	}

	public void fire()
	{
		for(int i = 0; i < num_enemies; i++)
		{
			Vector3 pos = floating_platform.instance.transform.position + Vector3.up * 6 + Vector3.forward * 8 + UnityEngine.Random.insideUnitSphere * 5;
			GameObject new_enemy = Instantiate(enemy_prefab, pos, Quaternion.identity) as GameObject;
		}

		Destroy (gameObject);
	}
}
