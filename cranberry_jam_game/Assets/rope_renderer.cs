﻿using UnityEngine;
using System.Collections;

public class rope_renderer : MonoBehaviour {

	public LineRenderer lr1;
	public Material mat;

	// Use this for initialization
	void Start () {
		lr1 = gameObject.AddComponent<LineRenderer>();
		lr1.SetWidth(0.2f, 0.2f);
		lr1.SetVertexCount(5);
		lr1.material = mat;

		if(floating_platform.instance == null)
			floating_platform.instance = GetComponent<floating_platform>();
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 bottom_of_balloon = transform.position - Vector3.up * transform.lossyScale.y * 0.5f;
		Vector3 offset_of_platform = Vector3.right * floating_platform.instance.transform.lossyScale.x * 0.5f;
		lr1.SetPosition(0, bottom_of_balloon);
		lr1.SetPosition(1, floating_platform.instance.transform.position + offset_of_platform);
		lr1.SetPosition(2, bottom_of_balloon);
		lr1.SetPosition(3, floating_platform.instance.transform.position - offset_of_platform);
		lr1.SetPosition(4, bottom_of_balloon);
	}
}
