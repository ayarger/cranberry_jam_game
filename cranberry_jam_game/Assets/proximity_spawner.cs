﻿using UnityEngine;
using System.Collections;

public class proximity_spawner : MonoBehaviour {

	public GameObject projectile_prefab;
	public GameObject target;
	public float proximity_distance = 10.0f;

	public float timer_max = 60.0f;
	public float timer;

	public Vector3 target_offset = Vector3.zero;

	public int projectile_count = 5;

	// Use this for initialization
	void Start () {
		timer = timer_max;
	}
	
	// Update is called once per frame
	void Update () {

		if(target == null)
			target = floating_platform.instance.gameObject;

		float distance = Vector3.Distance(transform.position, target.transform.position);

		if(distance <= proximity_distance)
		{
			timer -= 1;
			if(timer <= 0)
			{
				timer = timer_max;
				GameObject new_projectile = Instantiate(projectile_prefab, transform.position, Quaternion.identity) as GameObject;
				if(projectile_count > 0)
				{
					launchObject(new_projectile);
					projectile_count --;
				}
			}
		}
	}

	void launchObject(GameObject projectile)
	{
		float _angle = 45.0f;

		// source and target positions
		Vector3 pos = projectile.transform.position;
		Vector3 target_pos = target.transform.position + target_offset;
		
		// distance between target and source
		float dist = Vector3.Distance(pos, target_pos);
		
		// rotate the object to face the target
		transform.LookAt(target_pos);
		
		// calculate initival velocity required to land the cube on target using the formula (9)
		float Vi = Mathf.Sqrt(dist * -Physics.gravity.y / (Mathf.Sin(Mathf.Deg2Rad * _angle * 2)));
		float Vy, Vz;   // y,z components of the initial velocity
		
		Vy = Vi * Mathf.Sin(Mathf.Deg2Rad * _angle);
		Vz = Vi * Mathf.Cos(Mathf.Deg2Rad * _angle);
		
		// create the velocity vector in local space
		Vector3 localVelocity = new Vector3(0f, Vy, Vz);
		
		// transform it to global vector
		Vector3 globalVelocity = transform.TransformVector(localVelocity);
		
		// launch the cube by setting its initial velocity
		projectile.GetComponent<Rigidbody>().velocity = globalVelocity;

	}
}
