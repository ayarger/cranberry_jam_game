﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using InControl;

public abstract class Element
{
	public bool finished = false;
	public virtual void onAddedToQueue() {}
	public virtual void onActive() {}
	public virtual void update(float time_delta_fraction) {}
	public virtual void onRemove() {}
	public virtual void onCollision(Collision coll) {}
	public virtual void onTrigger(Collider other) {}
		
	public static void disruptElement(List<Element> ele_queue, Element ele)
	{
		if(ele_queue.Count > 0)
		{
			ele_queue[0].onRemove();
			ele_queue[0] = ele;
		}
		else
			ele_queue.Add(ele);
		
		ele_queue[0].onAddedToQueue();
		ele_queue[0].onActive();
	}
	
	public static void addElement(List<Element> ele_queue, Element ele)
	{
		ele_queue.Add(ele);
		if(ele_queue.Count == 1)
			ele.onActive();
	}

	public static void updateElements(List<Element> ele_queue)
	{
		Application.targetFrameRate = 60;
		if(ele_queue.Count > 0)
		{
			float time_delta_fraction = Time.deltaTime / (1.0f / Application.targetFrameRate);
			ele_queue[0].update(time_delta_fraction);
			
			if(ele_queue[0].finished)
			{
				ele_queue[0].onRemove();
				ele_queue.RemoveAt(0);
				if(ele_queue.Count > 0)
					ele_queue[0].onActive();
			}
		}
	}

	public static void collideElement(List<Element> ele_queue, Collision coll)
	{
		if(ele_queue.Count > 0)
			ele_queue[0].onCollision(coll);
	}

	public static void triggerElement(List<Element> ele_queue, Collider other)
	{
		if(ele_queue.Count > 0)
			ele_queue[0].onTrigger(other);
	}
}

public class ElementPunch : Element
{
	Player player;

	float stun_timer = 0;
	GameObject punch_prefab;
	GameObject punch_instance;

	// Punch causes a player to temporarily stop.
	public ElementPunch (Player player, float stun, GameObject punch_prefab)
	{
		this.punch_prefab = punch_prefab;
		this.player = player;
		this.stun_timer = stun;
	}

	public override void onActive()
	{
		player.current_state = Player.PlayerState.ATTACKING;
		Vector3 punch_position = player.transform.position + player.transform.forward;
		punch_instance = MonoBehaviour.Instantiate(punch_prefab, punch_position, Quaternion.identity) as GameObject;
		punch_instance.GetComponent<punch_object>().source_player = player;
		punch_instance.transform.forward = player.transform.forward;
		AudioSource.PlayClipAtPoint(player.punch_clip, Camera.main.transform.position);

	}

	public override void update(float time_delta_fraction)
	{
		punch_instance.GetComponent<Rigidbody>().velocity = floating_platform.instance.GetComponent<Rigidbody>().velocity;

		stun_timer -= time_delta_fraction;
		if(stun_timer <= 0)
		{
			Element.addElement(player.ele_queue, new ElementStandardControllerMovement(player));
			finished = true;
		}

		// Player death
		if(player.transform.position.y - floating_platform.instance.transform.position.y < -5 && player.current_state != Player.PlayerState.KO)
		{
			player.current_state = Player.PlayerState.KO;
			Element.disruptElement(player.ele_queue, new ElementPlayerDeathCamera(player, 30));
		}
	}

	public override void onRemove() 
	{
		MonoBehaviour.Destroy(punch_instance);
	}
}

public class ElementDive : Element
{
	Player player;
	
	float stun_timer = 0;
	Vector3 desired_position;
	Vector3 direction;
	float distance_per_frame = 0;

	// Punch causes a player to temporarily stop.
	public ElementDive (Player player, float stun)
	{
		this.player = player;
		this.stun_timer = stun;
	}
	
	public override void onActive()
	{
		TrailRenderer tr = player.gameObject.AddComponent<TrailRenderer>();
		tr.material = player.trail_material;
		tr.startWidth = 0.5f;
		tr.endWidth = 0.25f;

		desired_position = player.transform.position + player.transform.forward * 3;
		distance_per_frame = Vector3.Distance(player.transform.position, desired_position) * 6;
		direction = (desired_position - player.transform.position).normalized;
	}
	
	public override void update(float time_delta_fraction)
	{
		// Navigate
		player.GetComponent<Rigidbody>().velocity = direction * distance_per_frame + new Vector3(floating_platform.instance.GetComponent<Rigidbody>().velocity.x, 0, floating_platform.instance.GetComponent<Rigidbody>().velocity.z);

		// Stun
		stun_timer -= time_delta_fraction;

		if(stun_timer <= 0)
		{
			Element.addElement(player.ele_queue, new ElementStandardControllerMovement(player));
			finished = true;
		}
		
		// Player death
		if(player.transform.position.y - floating_platform.instance.transform.position.y < -5 && player.current_state != Player.PlayerState.KO)
		{
			player.current_state = Player.PlayerState.KO;
			Element.disruptElement(player.ele_queue, new ElementPlayerDeathCamera(player, 30));
		}
	}

	public override void onRemove ()
	{
		MonoBehaviour.Destroy(player.GetComponent<TrailRenderer>());
	}
}

public class ElementDizzied : Element
{
	Entity entity;
	Vector3 trajectory;
	float timer;
	
	float sinCounter = 0.0f;
	bool hit_ground = false;

	public ElementDizzied (Entity entity, Vector3 trajectory, float timer)
	{
		this.entity = entity;
		this.trajectory = trajectory;
		this.timer = timer;
	}
	
	public override void onActive()
	{
		entity.currentState = EntityState.STUNNED;
		entity.GetComponent<Rigidbody>().velocity = trajectory;
		entity.GetComponent<Rigidbody>().angularVelocity = UnityEngine.Random.onUnitSphere * 10;

		if(entity.GetComponent<enemyController>() != null)
			entity.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
	}
	
	public override void update(float time_delta_fraction)
	{
		if(!hit_ground)
			entity.GetComponent<Rigidbody>().velocity = new Vector3(floating_platform.instance.forward_movement.x + trajectory.x, entity.GetComponent<Rigidbody>().velocity.y ,floating_platform.instance.forward_movement.z + trajectory.z);

		timer -= time_delta_fraction;
		if(timer <= 0)
		{
			finished = true;
			if(entity.GetComponent<Player>() != null)
				Element.addElement(entity.GetComponent<Player>().ele_queue, new ElementStandardControllerMovement(entity.GetComponent<Player>()));
			else if(entity.GetComponent<enemyController>() != null)
				Element.addElement(entity.GetComponent<enemyController>().ele_queue, new ElementStandardEnemyMovement(entity.GetComponent<enemyController>()));
		}

		// Squish!
		sinCounter += 0.5f;
		entity.transform.localScale = new Vector3(1.0f + Mathf.Sin(sinCounter) * 0.1f, 1.0f + Mathf.Sin(sinCounter) * 0.1f, 1.0f + Mathf.Sin(sinCounter) * 0.1f);
	
		// Player death
		if(entity.GetComponent<Player>() != null)
		{
			if(entity.transform.position.y - floating_platform.instance.transform.position.y < -5 && entity.GetComponent<Player>().current_state != Player.PlayerState.KO)
			{
				entity.GetComponent<Player>().current_state = Player.PlayerState.KO;
				Element.disruptElement(entity.GetComponent<Player>().ele_queue, new ElementPlayerDeathCamera(entity.GetComponent<Player>(), 30));
			}
		}
	}

	public override void onRemove ()
	{
		entity.transform.localScale = Vector3.one;
		entity.GetComponent<Renderer>().material.color = entity.original_color;
	}

	public override void onCollision(Collision coll) 
	{
		if(coll.gameObject.tag == "player_platform")
		{
			hit_ground = true;
			entity.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
		}

		if(coll.gameObject.tag == "enemy" && entity.GetComponent<Player>() != null)
		{
			Vector3 trajectory = (entity.transform.position - coll.gameObject.transform.position) * 1;
			trajectory += Vector3.up * 1;
			
			Element.disruptElement(entity.GetComponent<Player>().ele_queue, new ElementDizzied(entity, trajectory, 60));
		}
	}

	public override void onTrigger(Collider other) 
	{
		if(other.gameObject.tag == "punch_object" && entity.GetComponent<Player>() == null)
		{
			Vector3 trajectory = (entity.transform.position - other.gameObject.GetComponent<punch_object>().source_player.transform.position).normalized * 1;
			trajectory += Vector3.up * 1;

			if(entity.GetComponent<enemyController>() != null)
				Element.disruptElement(entity.GetComponent<enemyController>().ele_queue, new ElementDizzied(entity, trajectory, 60));
		}
	}
}

public class ElementStandardControllerJump : Element
{
	Player player;
	Vector3 desired_looking_direction = Vector3.zero;

	float sinCounter = 0.0f;
	float sproing_affect_factor = 1.0f;

	public ElementStandardControllerJump (Player player)
	{
		this.player = player;
	}
	
	public override void onActive()
	{
		player.current_state = Player.PlayerState.NORMAL;
		player.GetComponent<Rigidbody>().velocity += Vector3.up * 5;
	}

	public override void update(float time_delta_fraction)
	{
		// Sproing
		sinCounter += 0.3f;
		sproing_affect_factor -= 0.05f;
		if(sproing_affect_factor > 0.0f)
		{
			sproing_affect_factor -= 0;
		}
		else
			sproing_affect_factor = 0.0f;

		player.transform.localScale = new Vector3(player.transform.localScale.x, 1.0f + Mathf.Sin(sinCounter) * 0.4f * sproing_affect_factor, player.transform.localScale.z);

		// BUG: sproing can result in superjump. Limit positive vertical velocity.
		if(player.GetComponent<Rigidbody>().velocity.y > 5.0f)
			player.GetComponent<Rigidbody>().velocity = new Vector3(player.GetComponent<Rigidbody>().velocity.x, 5.0f, player.GetComponent<Rigidbody>().velocity.z);

		var inputDevice = (InputManager.Devices.Count > player.player_number) ? InputManager.Devices[player.player_number] : null;
		if (inputDevice == null)
		{
			// If no controller exists for this cube, just make it translucent.
		}
		else
		{
			UpdateCubeWithInputDevice( inputDevice );
		}

		// Player death
		if(player.transform.position.y - floating_platform.instance.transform.position.y < -5 && player.current_state != Player.PlayerState.KO)
		{
			player.current_state = Player.PlayerState.KO;
			Element.disruptElement(player.ele_queue, new ElementPlayerDeathCamera(player, 30));
		}
	}

	public override void onRemove ()
	{
		player.transform.localScale = Vector3.one;
	}

	void UpdateCubeWithInputDevice( InputDevice inputDevice )
	{
		/*
		if(inputDevice.Action3)
		{
			if(!player.holding_attack)
			{
				player.holding_attack = true;
				player.current_state = Player.PlayerState.ATTACKING;
				
				Element.disruptElement(player.ele_queue, new ElementPunch(player, 5, player.punch_prefab));
			}
		}
		else
			player.holding_attack = false;*/
		
		/*Vector3 displacement = new Vector3(inputDevice.LeftStickX, 0, inputDevice.LeftStickY);
		if(displacement.magnitude > 0.01f)
		{
			desired_looking_direction = displacement;
		}*/
		
		player.transform.forward = Vector3.Slerp(player.transform.forward, desired_looking_direction, 0.25f);
		/*player.GetComponent<Rigidbody>().velocity += displacement * player.running_speed * 0.05f;
		Vector2 level_velocity = new Vector2(player.GetComponent<Rigidbody>().velocity.x, player.GetComponent<Rigidbody>().velocity.z);
		if(level_velocity.magnitude > player.running_speed)
		{
			level_velocity = level_velocity.normalized * player.running_speed;
			player.GetComponent<Rigidbody>().velocity = new Vector3(level_velocity.x, player.GetComponent<Rigidbody>().velocity.y, level_velocity.y);
		}*/

		//player.GetComponent<Rigidbody>().velocity = new Vector3(floating_platform.instance.GetComponent<Rigidbody>().velocity.x + displacement.x * player.running_speed ,player.GetComponent<Rigidbody>().velocity.y, floating_platform.instance.GetComponent<Rigidbody>().velocity.z + displacement.z * player.running_speed);
		Vector3 displacement = new Vector3(inputDevice.LeftStickX, 0, inputDevice.LeftStickY);
		if(displacement.magnitude > 0.01f)
		{
			desired_looking_direction = displacement;
		}
		
		player.transform.forward = Vector3.Slerp(player.transform.forward, desired_looking_direction, 0.25f);
		player.GetComponent<Rigidbody>().velocity = new Vector3(floating_platform.instance.GetComponent<Rigidbody>().velocity.x + displacement.x * player.running_speed ,player.GetComponent<Rigidbody>().velocity.y, floating_platform.instance.GetComponent<Rigidbody>().velocity.z + displacement.z * player.running_speed);
	}

	public override void onCollision(Collision coll) 
	{
		if(coll.gameObject.tag == "player_platform")
		{
			Element.disruptElement(player.ele_queue, new ElementStandardControllerMovement(this.player));
		}
	}
}

public class ElementStandardControllerMovement : Element
{
	Player player;
	Vector3 desired_looking_direction = Vector3.zero;

	public ElementStandardControllerMovement (Player player)
	{
		this.player = player;
	}

	public override void onActive()
	{
		player.current_state = Player.PlayerState.NORMAL;
	}

	public override void update(float time_delta_fraction)
	{
		var inputDevice = (InputManager.Devices.Count > player.player_number) ? InputManager.Devices[player.player_number] : null;

		if(player.player_number == 1 && !Player.player1_available)
			MonoBehaviour.Destroy(player.gameObject);
		if(player.player_number == 2 && !Player.player2_available)
			MonoBehaviour.Destroy(player.gameObject);
		if(player.player_number == 3 && !Player.player3_available)
			MonoBehaviour.Destroy(player.gameObject);
		if(player.player_number == 4 && !Player.player4_available)
			MonoBehaviour.Destroy(player.gameObject);

		if (inputDevice == null)
		{
			// If no controller exists for this cube, Destroy them.
			MonoBehaviour.Destroy(player.gameObject);
		}
		else
		{
			UpdateCubeWithInputDevice( inputDevice );
		}
	}

	void UpdateCubeWithInputDevice( InputDevice inputDevice )
	{
		// PUNCH
		if(inputDevice.Action3)
		{
			if(!player.holding_attack)
			{
				player.holding_attack = true;
				player.current_state = Player.PlayerState.ATTACKING;

				Element.disruptElement(player.ele_queue, new ElementPunch(player, 5, player.punch_prefab));
			}
		}
		else
			player.holding_attack = false;

		//JUMP
		if(inputDevice.Action1)
		{
			Element.disruptElement(player.ele_queue, new ElementStandardControllerJump(player));
		}

		//DIVE
		if(inputDevice.RightBumper)
		{
			Element.disruptElement(player.ele_queue, new ElementDive(player, 15));
		}

		// Player death
		if(player.transform.position.y - floating_platform.instance.transform.position.y < -5 && player.current_state != Player.PlayerState.KO)
		{
			player.current_state = Player.PlayerState.KO;
			Element.disruptElement(player.ele_queue, new ElementPlayerDeathCamera(player, 30));
		}

		// Movement
		Vector3 displacement = new Vector3(inputDevice.LeftStickX, 0, inputDevice.LeftStickY);
		if(displacement.magnitude > 0.01f)
		{
			desired_looking_direction = displacement;
		}

		player.transform.forward = Vector3.Slerp(player.transform.forward, desired_looking_direction, 0.45f);
		player.GetComponent<Rigidbody>().velocity = new Vector3(floating_platform.instance.GetComponent<Rigidbody>().velocity.x + displacement.x * player.running_speed ,player.GetComponent<Rigidbody>().velocity.y, floating_platform.instance.GetComponent<Rigidbody>().velocity.z + displacement.z * player.running_speed);
	}

	public override void onCollision(Collision coll) 
	{
		if(coll.gameObject.tag == "enemy")
		{
			Vector3 trajectory = (player.transform.position - coll.gameObject.transform.position) * 2;
			trajectory += Vector3.up * 1;

			Element.disruptElement(player.ele_queue, new ElementDizzied(player.GetComponent<Entity>(), trajectory, 90));
		}
	}

	public override void onTrigger(Collider other) 
	{/*
		if(other.gameObject.tag == "punch_object")
		{
			Vector3 trajectory = (player.transform.position - other.gameObject.GetComponent<punch_object>().source_player.transform.position).normalized * 5;
			trajectory += Vector3.up * 3;
			
			Element.disruptElement(player.ele_queue, new ElementDizzied(player.GetComponent<Entity>(), trajectory, 90));
		}*/
	}
}

public class ElementPlayerDeathCamera : Element
{
	float timer;
	float timer2 = 60;
	Player player;

	public ElementPlayerDeathCamera (Player player, float timer)
	{
		this.player = player;
		this.timer = timer;
	}

	public override void onActive()
	{
		Time.timeScale = 0.3F;
		look_at_target lat = Camera.main.gameObject.AddComponent<look_at_target>();
		lat.ease = 0.1f;
		lat.target = player.gameObject;
		AudioSource.PlayClipAtPoint(player.falling_clip, Camera.main.transform.position);

	}
	
	public override void update(float time_delta_fraction)
	{
		timer -= time_delta_fraction;
		if(timer <= 0)
		{
			Time.timeScale = 1.0F;
			if(Camera.main.gameObject.GetComponent<look_at_target>() != null)
				MonoBehaviour.Destroy(Camera.main.gameObject.GetComponent<look_at_target>());

			// MOVE CAMERA BACK
			timer2 -= time_delta_fraction;
			Camera.main.transform.forward = Vector3.Slerp(Camera.main.transform.forward, new Vector3(0.0f, -0.3f, 0.9f), 0.1f);
			if(timer2 <= 0)
			{
				Element.disruptElement(player.ele_queue, new ElementNoop(360));
				// Respawn player in middle of platform.
				Element.addElement(player.ele_queue, new ElementExecuteAction(() => { player.transform.position = floating_platform.instance.transform.position + Vector3.up * 3;
																				player.current_state = Player.PlayerState.KO; }));
				Element.addElement(player.ele_queue, new ElementStandardControllerMovement(player));
				finished = true;
			}
		}
	}
}

public class ElementStandardKeyboardMovement : Element
{
	Player player;
	public ElementStandardKeyboardMovement (Player player)
	{
		this.player = player;
	}
	
	public override void update(float time_delta_fraction)
	{
		Vector3 movement = Vector3.zero;
		
		if(player.player_number == 1)
		{
			if(Input.GetKey(KeyCode.RightArrow))
				movement += new Vector3(player.running_speed, 0, 0);
			if(Input.GetKey(KeyCode.LeftArrow))
				movement += new Vector3(-player.running_speed, 0, 0);
			if(Input.GetKey(KeyCode.UpArrow))
				movement += new Vector3(0, 0, player.running_speed * 0.8f);
			if(Input.GetKey(KeyCode.DownArrow))
				movement += new Vector3(0, 0, -player.running_speed * 0.8f);
		}
		else if (player.player_number == 2)
		{
			if(Input.GetKey(KeyCode.D))
				movement += new Vector3(player.running_speed, 0, 0);
			if(Input.GetKey(KeyCode.A))
				movement += new Vector3(-player.running_speed, 0, 0);
			if(Input.GetKey(KeyCode.W))
				movement += new Vector3(0, 0, player.running_speed * 0.8f);
			if(Input.GetKey(KeyCode.S))
				movement += new Vector3(0, 0, -player.running_speed * 0.8f);
		}
		
		player.transform.position += movement * time_delta_fraction;
		
		if(movement.x > 0)
			player.transform.localScale = new Vector3(-1, 1, 1);
		if(movement.x < 0)
			player.transform.localScale = new Vector3(1, 1, 1);
		
		if(movement.magnitude > 0)
			player.GetComponent<Animator>().SetBool("is_moving", true);
		else
			player.GetComponent<Animator>().SetBool("is_moving", false);
		
		// Jump
		if(Input.GetKeyDown(KeyCode.Space))
			Element.disruptElement(player.ele_queue, new ElementJump(player, new Vector3(movement.x * 40, 20, movement.z)));
	}
}


public class ElementAddElement : Element
{
	List<Element> ElementQueue;
	Element ele;
	
	public ElementAddElement(List<Element> ElementQueue, Element ele) 
	{ 
		this.ElementQueue = ElementQueue;
		this.ele = ele;
	}
	
	public override void onActive()
	{
		Element.addElement(ElementQueue, ele);
		finished = true;
	}
}

public class ElementDisruptElement : Element
{
	List<Element> ElementQueue;
	Element ele;
	
	public ElementDisruptElement(List<Element> ElementQueue, Element ele) 
	{ 
		this.ElementQueue = ElementQueue;
		this.ele = ele;
	}
	
	public override void onActive()
	{
		Element.disruptElement(ElementQueue, ele);
		finished = true;
	}
}

public class ElementKo : Element
{
	Player player;

	public ElementKo(Player player) 
	{ 
		this.player = player;
	}
	
	public override void onActive()
	{
		player.GetComponent<Player>().current_state = Player.PlayerState.KO;
		player.GetComponent<Animator>().enabled = false;
		//player.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
		player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;
		player.GetComponent<Rigidbody>().useGravity = true;
		player.GetComponent<Rigidbody>().angularDrag = 0.0f;
		player.GetComponent<Rigidbody>().angularVelocity = UnityEngine.Random.onUnitSphere;
		player.transform.Rotate(new Vector3(45, 0, 0));
		player.GetComponent<Renderer>().material.color = Color.red;
	}
	
	public override void update(float time_delta_fraction)
	{
		//life -= time_delta_fraction;
		//if(life <= 0)
			//finished = true;
	}
}



public class ElementJump : Element
{
	Player player;
	Vector3 tranjectory = Vector3.zero;

	public ElementJump(Player player, Vector3 trajectory) 
	{ 
		this.player = player;
		this.tranjectory = trajectory;
	}
	
	public override void onActive()
	{
		player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation;
		//player.GetComponent<ground_controller>().enabled = false;
		player.GetComponent<Rigidbody>().useGravity = true;
		player.GetComponent<Rigidbody>().velocity += tranjectory;
	}
	
	public override void update(float time_delta_fraction)
	{
		RaycastHit hit = new RaycastHit();
		if (Physics.Raycast(player.transform.position, -Vector3.up, out hit, 6)) {
			if(player.GetComponent<Rigidbody>().velocity.y <= 0)
				finished = true;
		}

		if(Input.GetKeyUp(KeyCode.Space) && !finished && player.GetComponent<Rigidbody>().velocity.y > 0)
		{
			//print("hi");
			player.GetComponent<Rigidbody>().velocity = new Vector3(player.GetComponent<Rigidbody>().velocity.x,
			                                                        0, 
			                                                        player.GetComponent<Rigidbody>().velocity.z);
		}
	}
	
	public override void onRemove()
	{
		player.GetComponent<Rigidbody>().useGravity = false;
		//player.GetComponent<ground_controller>().enabled = true;
		player.GetComponent<Renderer>().material.color = Color.white;
		player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;
	}
}

/*
 * This element is responsible for handling an entity in its "Stunned" state.
 */
public class ElementStunned : Element
{
	Entity entity;
	float life = 0.0f;
	bool shouldSpin;
	
	public ElementStunned(Entity entity, float stunTicks, bool shouldSpin)
	{ 
		this.entity = entity;
		life = stunTicks;
		this.shouldSpin = shouldSpin;
	}
	
	public override void onActive()
	{
		entity.currentState = EntityState.STUNNED;
		// Color the object Red to indicate damage.
		entity.GetComponent<Renderer>().material.color = new Color(1, 0, 0);
		
		// If the stunned entity should spin, adjust constraints, and provide an angular velocity
		// for a fun physics effect.
		if(shouldSpin)
		{
			entity.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ;
			entity.GetComponent<Rigidbody>().angularVelocity = UnityEngine.Random.onUnitSphere * 10;
		}
	}
	
	public override void update(float time_delta_fraction)
	{
		life -= time_delta_fraction;
		
		// Setting 'finished' to true ends the Element.
		if(life <= 0)
			finished = true;
	}
	
	public override void onRemove()
	{
		entity.currentState = EntityState.NORMAL;
		
		// Return the entity's color to normal.
		entity.GetComponent<Renderer>().material.color = Color.white;
		
		// Reconfigure the entity's physics constraints.
		if(shouldSpin)
		{
			entity.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;
			entity.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
			
			Quaternion q = new Quaternion();
			q.eulerAngles = Vector3.zero;
			entity.transform.rotation = q;
		}
	}
}

public class ElementDodge : Element
{
	Player player;
	Vector3 dest;
	float life = 0;
	
	public ElementDodge(Player player, Vector3 dest, int stunTicks) 
	{ 
		this.player = player;
		life = stunTicks; 
		this.dest = dest;
	}
	
	public override void onActive()
	{
	}
	
	public override void update(float time_delta_fraction)
	{
		life -= time_delta_fraction;
		if(life <= 0)
			finished = true;

		player.GetComponent<Rigidbody>().velocity = (dest - player.transform.position) * 20f * time_delta_fraction;
	}
}


public class ElementNoop : Element
{
	float life = 0;
	
	public ElementNoop(float stunTicks)
	{ 
		life = stunTicks; 
	}
	
	public override void update(float time_delta_fraction)
	{
		life -= time_delta_fraction;
		if(life <= 0)
			finished = true;
	}
}

public class ElementMoveOverTime : Element
{
	float total_life = 0;
	float life = 0;
	GameObject my_object;
	Vector3 destination;
	Vector3 initial_position;
	bool local;

	public ElementMoveOverTime(float stunTicks, Vector3 dest, Vector3 initial, GameObject my_object, bool local)
	{ 
		this.total_life = stunTicks;
		this.my_object = my_object;
		this.destination = dest;
		this.initial_position = initial;
		this.local = local;
	}
	
	public override void update(float time_delta_fraction)
	{
		life += time_delta_fraction;
		float ratio = (float)life / total_life;

		if(local)
			my_object.transform.localPosition = Vector3.Lerp(initial_position, destination, ratio);
		else
			my_object.transform.position = Vector3.Lerp(initial_position, destination, ratio);

		if(ratio >= 1.0f)
			finished = true;
	}
}

public enum MovementType {ABSOLUTE, LOCAL, UIABSOLUTE, UILOCAL};

public class ElementMoveEase : Element
{
	float easeFactor = 0;
	GameObject my_object;
	Vector3 destination;
	Vector3 initial_position;
	MovementType movementType;

	public ElementMoveEase(Vector3 dest, float easeFactor, GameObject my_object, MovementType movementType)
	{ 
		this.my_object = my_object;
		this.destination = dest;
		this.easeFactor = easeFactor;
		this.movementType = movementType;
	}
	
	public override void update(float time_delta_fraction)
	{
		Vector3 diff = Vector3.zero;
		
		if(movementType == MovementType.LOCAL)
		{
			diff = (destination - my_object.transform.localPosition) * easeFactor;
			my_object.transform.localPosition += diff;
		}
		else if(movementType == MovementType.ABSOLUTE)
		{
			diff = (destination - my_object.transform.position) * easeFactor;
			my_object.transform.position += diff;
		} else if(movementType == MovementType.UIABSOLUTE)
		{
			Vector2 desiredPosition = new Vector2(destination.x, destination.y);
			Vector2 diff2 = (desiredPosition - my_object.GetComponent<RectTransform>().anchoredPosition) * easeFactor;
			my_object.GetComponent<RectTransform>().anchoredPosition += diff2;
			diff = new Vector3(diff2.x, diff2.y, 0);
		}

		if(diff.magnitude <= 0.01f)
			finished = true;
	}
}


public class ElementMoveRotateOverTime : Element
{
	int total_life = 0;
	int life = 0;
	GameObject my_object;
	Vector3 destination_pos;
	Vector3 initial_position;
	Quaternion initial_rot;
	Quaternion dest_rot;
	bool local;
	
	public ElementMoveRotateOverTime(int stunTicks, Vector3 dest_pos, Vector3 initial_pos, Vector3 dest_rot, Vector3 init_rot,
	                                 GameObject my_object, bool local)
	{ 
		this.total_life = stunTicks;
		this.my_object = my_object;
		this.destination_pos = dest_pos;
		this.initial_position = initial_pos;
		this.local = local;

		Quaternion q_init = new Quaternion();
		q_init.eulerAngles = init_rot;
		this.initial_rot = q_init;

		Quaternion q_dest = new Quaternion();
		q_dest.eulerAngles = dest_rot;
		this.dest_rot = q_dest;
	}
	
	public override void update(float time_delta_fraction)
	{
		life ++;
		float ratio = (float)life / total_life;
		
		if(local)
		{
			my_object.transform.localPosition = Vector3.Lerp(initial_position, destination_pos, ratio);
			my_object.transform.localRotation = Quaternion.Slerp(initial_rot, dest_rot, ratio);
		}
		else
		{
			my_object.transform.position = Vector3.Lerp(initial_position, destination_pos, ratio);
			my_object.transform.rotation = Quaternion.Slerp(initial_rot, dest_rot, ratio);
		}
		
		if(ratio >= 1.0f)
			finished = true;
	}
}

public class ElementWarpObject : Element
{
	GameObject my_object;
	Vector3 destination;
	Quaternion direction;
	MovementType movementType;

	public ElementWarpObject(Vector3 dest, Vector3 dir, GameObject my_object, MovementType movementType)
	{ 
		this.my_object = my_object;
		this.destination = dest;
		Quaternion q = new Quaternion();
		q.eulerAngles = dir;
		direction = q;
		this.movementType = movementType;
	}
	
	public override void onActive()
	{
		MonoBehaviour.print("WARP");

		if(movementType == MovementType.ABSOLUTE)
			my_object.transform.localPosition = destination;
		else if(movementType == MovementType.LOCAL)
			my_object.transform.position = destination;
		else if(movementType == MovementType.UIABSOLUTE)
			my_object.GetComponent<RectTransform>().anchoredPosition = destination;
		my_object.transform.rotation = direction;
		finished = true;
	}
}

public class ElementLoadScene : Element
{
	string scene_name;
	
	public ElementLoadScene(string scene_name)
	{ 
		this.scene_name = scene_name;
	}
	
	public override void onActive()
	{
		Application.LoadLevel(scene_name);
	}
}

public class ElementSetTextMesh : Element
{
	TextMesh my_mesh;
	string my_message;
	
	public ElementSetTextMesh(TextMesh t, string s)
	{ 
		this.my_mesh = t;
		this.my_message = s;
	}
	
	public override void onActive()
	{
		MonoBehaviour.print("TEXTMESH");
		my_mesh.text = my_message;
		finished = true;
	}
}

public class ElementOnOffCamera : Element
{
	Camera cam;
	bool on;
	
	public ElementOnOffCamera(Camera cam, bool on)
	{ 
		this.cam = cam;
		this.on = on;
	}
	
	public override void onActive ()
	{
		cam.enabled = on;
		finished = true;
	}
}

public class ElementInstantiateObject : Element
{
	GameObject prefab;
	Vector3 pos;
	Quaternion rot;
	
	public ElementInstantiateObject(GameObject prefab, Vector3 pos, Quaternion rot)
	{ 
		this.prefab = prefab;
		this.pos = pos;
		this.rot = rot;
	}
	
	public override void onActive ()
	{
		MonoBehaviour.Instantiate(prefab, pos, rot);
		finished = true;
	}
}

public class ElementExecuteAction : Element
{
	Action act;
	
	public ElementExecuteAction (Action act)
	{
		this.act = act;
	}
	
	public override void onActive()
	{
		act();
		finished = true;
	}
}

public class ElementFadeScreen : Element
{	
	GameObject fade_object;
	float initial_fade = 0.0f;
	float fade_goal = 1.0f;
	float ease_factor = 0.1f;
	string type;
	public ElementFadeScreen (GameObject fade_object, float initial_fade, float fade_goal, float ease_factor, string type)
	{
		this.fade_object = fade_object;
		this.initial_fade = initial_fade;
		this.fade_goal = fade_goal;
		this.ease_factor = ease_factor;
		this.type = type;
	}
	
	public override void onActive() 
	{
		if(type == "panel")
		{
			Color current_color = fade_object.GetComponent<Image>().color;
			fade_object.GetComponent<Image>().color = new Color(current_color.r,
			                                                    current_color.g,
			                                                    current_color.b,
			                                                    initial_fade);
		}
		else if(type == "text")
		{
			Color current_color = fade_object.GetComponent<Text>().color;
			fade_object.GetComponent<Text>().color = new Color(current_color.r,
			                                                    current_color.g,
			                                                    current_color.b,
			                                                    initial_fade);
		}
	}
	
	public override void update(float time_delta_fraction)
	{
		// BORDERS

		if(type == "panel")
		{
			Color current_color = fade_object.GetComponent<Image>().color;
			fade_object.GetComponent<Image>().color = new Color(current_color.r,
			                                                    current_color.g,
			                                                    current_color.b,
			                                                    current_color.a + (fade_goal - current_color.a) * ease_factor * time_delta_fraction);
			
			if(fade_object.GetComponent<Image>().color.a >= fade_goal - 0.02f)
			{
				fade_object.GetComponent<Image>().color = new Color(current_color.r,
				                                                    current_color.g,
				                                                    current_color.b,
				                                                    fade_goal);
				finished = true;
			}
		}
		else if(type == "text")
		{
			Color current_color = fade_object.GetComponent<Text>().color;
			fade_object.GetComponent<Text>().color = new Color(current_color.r,
			                                                    current_color.g,
			                                                    current_color.b,
			                                                    current_color.a + (fade_goal - current_color.a) * ease_factor * time_delta_fraction);
			
			if(fade_object.GetComponent<Text>().color.a >= fade_goal - 0.02f)
			{
				fade_object.GetComponent<Text>().color = new Color(current_color.r,
				                                                    current_color.g,
				                                                    current_color.b,
				                                                    fade_goal);
				finished = true;
			}
		}
	}
}

public class ElementDialogue : Element
{
	string desired_text_content = "";
	string current_text_content = "";
	Text text_object;
	GameObject character_object;
	TextAnchor alignment;
	Sprite face_image;
	AudioClip text_clip;
	
	int max_character_timer = 0;
	int character_timer = 0;
	
	List<char> mildPauseCharacters = new List<char>(){ ',', '-', };
	List<char> longPauseCharacters = new List<char>(){ '.', '?', '!'};
	
	public ElementDialogue (string text_content, int max_character_timer, TextAnchor alignment, Text text_object, GameObject character_object, Sprite face_image, AudioClip text_clip)
	{
		this.text_object = text_object;
		this.alignment = alignment;
		this.desired_text_content = text_content;
		this.max_character_timer = max_character_timer;
		this.character_object = character_object;
		this.face_image = face_image;
	}
	
	public override void onActive()
	{	
		text_object.text = "";
		text_object.alignment = alignment;
		character_timer = max_character_timer;
		
		
		if(max_character_timer == -1)
		{
			text_object.text = desired_text_content;
			current_text_content = desired_text_content;
			finished = true;
		}
		else
		{
			if(character_object != null)
			{
				character_object.GetComponent<breath_effect>().rate = 3.0f;
				character_object.GetComponent<breath_effect>().amplitude = 0.05f;
			}
		}
	}
	
	public override void update(float time_delta_fraction)
	{
		character_timer --;
		if(character_timer <= 0)
		{
			character_timer = max_character_timer;
			if(current_text_content.Length < desired_text_content.Length)
			{
				char nextChar = desired_text_content[current_text_content.Length];
				current_text_content += nextChar;
				
				Camera.main.GetComponent<AudioSource>().PlayOneShot(text_clip, 1.0f);
				text_object.text = current_text_content;
				
				
				if(mildPauseCharacters.Contains(nextChar))
					character_timer += 10;
				else if(longPauseCharacters.Contains(nextChar))
					character_timer += 20;
			}
			else
			{
				finished = true;
			}
		}
	}
	
	public override void onRemove() 
	{
		if(character_object != null)
		{
			character_object.GetComponent<breath_effect>().rate = character_object.GetComponent<breath_effect>().initial_rate;
			character_object.GetComponent<breath_effect>().amplitude = character_object.GetComponent<breath_effect>().initial_amplitude;
		}
	}
}

/*
 * This element handles the 'chasing' behavior witnessed in enemies.
 */
public class ElementChase : Element
{
	float acceleration;
	
	Entity pursuer;
	Entity target;
	
	public ElementChase(Entity pursuer, Entity target, float acceleration)
	{
		this.pursuer = pursuer;
		this.target = target;
		this.acceleration = acceleration;
	}
	
	public override void update(float time_delta_fraction)
	{
		// Don't chase a stunned target.
		if(target.currentState == EntityState.STUNNED)
		{
			return;
		}
		
		Vector3 direction = (target.transform.position - pursuer.transform.position).normalized;
		pursuer.GetComponent<Rigidbody>().velocity += direction * acceleration;
	}
}

/*
 * This Element handles the Spin Attack behavior witnessed in enemies.
 */
public class ElementSpinAttack : Element
{
	float acceleration;
	
	Entity pursuer;
	Entity target;
	
	float timer = 360;
	
	public ElementSpinAttack(Entity pursuer, Entity target, float acceleration)
	{
		this.pursuer = pursuer;
		this.target = target;
		this.acceleration = acceleration;
	}
	
	public override void onActive()
	{
		pursuer.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY;
		
		// An entity performing a spin attack, may not be damaged.
		pursuer.currentState = EntityState.INVINCIBLE;
		pursuer.GetComponent<Rigidbody>().mass = 10;
		pursuer.GetComponent<Renderer>().material.color = Color.blue;
		pursuer.transform.localScale = Vector3.one * 1.25f;
	}
	
	public override void update(float time_delta_fraction)
	{
		timer -= time_delta_fraction;
		if(timer <= 0)
			finished = true;
		
		// Spin Like Crazy.
		pursuer.GetComponent<Rigidbody>().angularVelocity = new Vector3(0, 0, 10000);
		
		// Don't chase a stunned target.
		if(target.currentState == EntityState.STUNNED)
		{
			return;
		}
		
		Vector3 direction = (target.transform.position - pursuer.transform.position).normalized;
		pursuer.GetComponent<Rigidbody>().velocity += direction * acceleration;
	}
	
	public override void onRemove()
	{
		pursuer.GetComponent<Rigidbody>().mass = 1;
		pursuer.currentState = EntityState.NORMAL;
		pursuer.GetComponent<Renderer>().material.color = Color.white;
		pursuer.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation | RigidbodyConstraints.FreezePositionZ;
		pursuer.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
		
		Quaternion q = new Quaternion();
		q.eulerAngles = Vector3.zero;
		pursuer.transform.rotation = q;
		pursuer.transform.localScale = Vector3.one;
	}
}


