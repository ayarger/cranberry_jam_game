﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

	public int player_number = -1;
	public float running_speed = 4.0f;

	public List<Element> ele_queue = new List<Element>();

	public enum PlayerState { NORMAL, STUNNED, ATTACKING, KO, DEAD, INVENTORY, INTRO, GOAL };
	public PlayerState current_state = PlayerState.NORMAL;

	public static List<Player> instances = new List<Player>();

	public GameObject punch_prefab;

	public bool holding_attack = false;

	public Color original_color;

	public Material trail_material;

	public static bool player1_available = false;
	public static bool player2_available = false;
	public static bool player3_available = false;
	public static bool player4_available = false;

	public AudioClip punch_clip;
	public AudioClip falling_clip;

	// Use this for initialization
	void Start () {
		instances.Add(this);
		set_color();
		Element.addElement(ele_queue, new ElementStandardControllerMovement(this));
	}
	
	// Update is called once per frame
	void Update () {
		Element.updateElements(ele_queue);
	}

	void OnCollisionEnter(Collision coll)
	{
		Element.collideElement(ele_queue, coll);
	}

	void OnTriggerEnter(Collider other)
	{
		Element.triggerElement(ele_queue, other);
	}

	void set_color()
	{
		if(player_number == 1)
		{
			original_color = new Color(0, 1, 0);
		}
		else if(player_number == 2)
		{
			original_color = new Color(0, 0, 1);
		}
		else if(player_number == 3)
		{
			original_color = new Color(0, 1, 1);
		}
		else if(player_number == 4)
		{
			original_color = new Color(0, 0, 0);
		}

		GetComponent<Renderer>().material.color = original_color;
	}
}
