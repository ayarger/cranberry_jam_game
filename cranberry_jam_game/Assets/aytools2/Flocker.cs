﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Flocker : MonoBehaviour {

	static List<GameObject> flockers = new List<GameObject>();

	public int flockTag = -1;
	public float cohesion_weight = 1.0f;
	public float alignment_weight = 1.0f;
	public float separation_weight = 1.0f;
	public float ignore_radius = 10.0f;
	public float flocking_weight = 1.0f;

	Vector3 rand_dir = Vector3.zero;

	void Start () {
		rand_dir = UnityEngine.Random.onUnitSphere;

		flockers.Add(gameObject);
	}
	
	void Update () {
		Vector3 flocking_vector = computeAlignment() * alignment_weight 
								  + computeCohesion() * cohesion_weight
								  + computeSeparation() * separation_weight;

		GetComponent<Rigidbody>().velocity += (flocking_vector * flocking_weight - GetComponent<Rigidbody>().velocity) * 0.01f;

		print ("FLOCKING VECTOR: " + (flocking_vector * flocking_weight).ToString());
		//GetComponent<Rigidbody>().velocity = (flocking_vector * flocking_weight) + UnityEngine.Random.onUnitSphere * 0.1f;
	}

	Vector3 computeAlignment()
	{
		int num_nearby_flockers = 0;
		Vector3 result = Vector3.zero;

		foreach(GameObject go in flockers)
		{
			if(go == gameObject || go.GetComponent<Flocker>().flockTag != flockTag || Vector3.Distance(go.transform.position, transform.position) > ignore_radius)
				continue;

			num_nearby_flockers ++;
			Rigidbody r = go.GetComponent<Rigidbody>();
			result += r.velocity;
		}

		result /= num_nearby_flockers;
		return result.normalized;
	}

	Vector3 computeCohesion()
	{
		int num_nearby_flockers = 0;
		Vector3 result = Vector3.zero;
		
		foreach(GameObject go in flockers)
		{
			if(go == gameObject || go.GetComponent<Flocker>().flockTag != flockTag || Vector3.Distance(go.transform.position, transform.position) > ignore_radius)
				continue;

			num_nearby_flockers ++;
			Transform t = go.GetComponent<Transform>();
			result += t.position;
		}

		result /= num_nearby_flockers;

		result = new Vector3(result.x - transform.position.x, 
		                     result.y - transform.position.y, 
		                     result.z - transform.position.z);

		return result.normalized;
	}

	Vector3 computeSeparation()
	{
		int num_nearby_flockers = 0;
		Vector3 result = Vector3.zero;

		foreach(GameObject go in flockers)
		{
			if(go == gameObject || go.GetComponent<Flocker>().flockTag != flockTag || Vector3.Distance(go.transform.position, transform.position) > ignore_radius)
				continue;

			num_nearby_flockers ++;
			result += transform.position - go.transform.position;
		}

		result /= num_nearby_flockers;
		return result.normalized;
	}

	void OnDestroy()
	{
		flockers.Remove(gameObject);
	}
}
