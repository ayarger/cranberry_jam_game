﻿/*
 * Entity is a base class for "living" objects in the game.
 * 
 * "Living" objects have health, may be stunned, or made invincible.
 */

using UnityEngine;
using System.Collections;

public enum EntityState {NORMAL, STUNNED, INVINCIBLE};

public class Entity : MonoBehaviour {

	public int initialHealth = 3;
	public int health;

	public EntityState currentState = EntityState.NORMAL;
	public Color original_color;

	// This function provides a way for Derived objects run logic during Awake(),
	// While reserving Awake() itself for this base Entity object.
	protected virtual void PostAwake() {}

	// Use this for initialization
	void Awake () {
		health = initialHealth;
		PostAwake();
	}

	void Start()
	{
		set_color();
	}

	void set_color()
	{
		// Enemy coloring.
		original_color = new Color(1, 1, 1);

		// Player colorings.
		if(GetComponent<Player>() != null)
		{
			if(GetComponent<Player>().player_number == 1)
			{
				original_color = new Color(0, 1, 0);
			}
			else if(GetComponent<Player>().player_number == 2)
			{
				original_color = new Color(0, 0, 1);
			}
			else if(GetComponent<Player>().player_number == 3)
			{
				original_color = new Color(0, 1, 1);
			}
			else if(GetComponent<Player>().player_number == 4)
			{
				original_color = new Color(0, 0, 0);
			}
		}
		
		GetComponent<Renderer>().material.color = original_color;
	}
}