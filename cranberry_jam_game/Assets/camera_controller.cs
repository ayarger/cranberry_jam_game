﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class camera_controller : MonoBehaviour {

	List<Element> ele_queue = new List<Element>();

	public GameObject fade_screen;
	public GameObject game_over_text;
	public GameObject press_start_text;

	public static camera_controller instance;

	// Use this for initialization
	void Start () {
		Element.addElement(ele_queue, new ElementFadeScreen(fade_screen, 0, 1.0f, 1.0f, "panel"));
		Element.addElement(ele_queue, new ElementNoop(120));
		Element.addElement(ele_queue, new ElementFadeScreen(fade_screen, 1.0f, 0.0f, 0.02f, "panel"));

		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		Element.updateElements(ele_queue);
	}

	public void game_over()
	{
		Element.addElement(ele_queue, new ElementFadeScreen(fade_screen, 0, 1.0f, 0.02f, "panel"));
		Element.addElement(ele_queue, new ElementFadeScreen(game_over_text, 0, 1.0f, 0.1f, "text"));
		Element.addElement(ele_queue, new ElementFadeScreen(press_start_text, 0, 1.0f, 0.1f, "text"));
		Element.addElement(ele_queue, new ElementNoop(120));
		Element.addElement(ele_queue, new ElementLoadScene("Title"));
	}

	public void to_boss_scene()
	{
		Element.addElement(ele_queue, new ElementFadeScreen(fade_screen, 0, 1.0f, 0.02f, "panel"));
		Element.addElement(ele_queue, new ElementNoop(120));
		Element.addElement(ele_queue, new ElementLoadScene("BossCutscene"));
	}
}
