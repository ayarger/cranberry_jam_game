﻿using UnityEngine;
using System.Collections;

public class random_color : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Color newColor = new Color( Random.value, Random.value, Random.value, 1.0f );
		// apply it on current object's material
		GetComponent<Renderer>().material.color = newColor;
		GetComponent<Renderer> ().material.SetColor ("_EmissionColor", newColor*500f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
