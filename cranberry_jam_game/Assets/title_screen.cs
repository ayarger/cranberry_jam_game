﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using InControl;
using XInputDotNetPure;

public class title_screen : MonoBehaviour {

	public Text p1text;
	public Text p2text;
	public Text p3text;
	public Text p4text;

	public AudioClip confirmclip;

	/*
	public PlayerIndex playerIndex;
	private GamePadState state;
	*/
	
	void Start()
	{
		Player.player1_available = false;
		Player.player2_available = false;
		Player.player3_available = false;
		Player.player4_available = false;
	}

	// Update is called once per frame
	void Update () {

		for(int i = 0; i < InputManager.Devices.Count; i++)
		{
			var inputDevice = InputManager.Devices[i];
			if(inputDevice.Action1)
			{
				if(i == 1 && !Player.player1_available)
				{
					AudioSource.PlayClipAtPoint(confirmclip, Camera.main.transform.position);
					Player.player1_available = true;
				}
				if(i == 2 && !Player.player2_available)
				{
					AudioSource.PlayClipAtPoint(confirmclip, Camera.main.transform.position);

					Player.player2_available = true;
				}
				if(i == 3 && !Player.player3_available)
				{
					AudioSource.PlayClipAtPoint(confirmclip, Camera.main.transform.position);

					Player.player3_available = true;
				}
				if(i == 4 && !Player.player4_available)
				{
					AudioSource.PlayClipAtPoint(confirmclip, Camera.main.transform.position);

					Player.player4_available = true;
				}
			}

			if(inputDevice.Action4)
			{
				if(Player.player1_available || Player.player2_available || Player.player3_available || Player.player4_available)
					Application.LoadLevel("austin_scene_2");
			}

			if(inputDevice.Action3)
			{
				if(Player.player1_available || Player.player2_available || Player.player3_available || Player.player4_available)
					Application.LoadLevel("Boss");
			}



			//XIn
			/*
			state = GamePad.GetState(playerIndex);
			for(i=0, i<4, i++){
            	if (state.Buttons.A == ButtonState.Pressed)
					*/
		}

		// TEXTS
		if(Player.player1_available)
			p1text.text = "P1 READY!";
		else
			p1text.text = "Press X to JOIN!";

		if(Player.player2_available)
			p2text.text = "P2 READY!";
		else
			p2text.text = "Press X to JOIN!";

		if(Player.player3_available)
			p3text.text = "P3 READY!";
		else
			p3text.text = "Press X to JOIN!";

		if(Player.player4_available)
			p4text.text = "P4 READY!";
		else
			p4text.text = "Press X to JOIN!";
	}
}
