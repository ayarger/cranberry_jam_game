﻿using UnityEngine;
using System.Collections;

public class follow_target : MonoBehaviour {

	public GameObject target;
	public Vector3 offset;
	public float ease;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.position += (target.transform.position + offset - transform.position) * ease;
	}
}
