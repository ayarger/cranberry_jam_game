﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class cena : MonoBehaviour {

	public List<Element> ele_queue = new List<Element>();
	public Vector3 chase_offset = Vector3.zero;

	public int health = 25;
	public int invincibility = 0;

	public Color original_color;

	public float sinCounter = 0.0f;

	public GameObject enemy_prefab;

	public ParticleSystem ps;

	public GameObject cena_beam_prefab;

	public Vector3 cena_beam_offset;

	// Use this for initialization
	void Start () {
		Element.addElement(ele_queue, new ElementStandardBoss(this));
	}
	
	// Update is called once per frame
	void Update () {
		Element.updateElements(ele_queue);

		invincibility --;
		sinCounter += 0.4f;
		if(invincibility >= 0)
		{
			transform.localScale = Vector3.one * 10 + Vector3.one * Mathf.Sin(sinCounter);
		}
		else
			transform.localScale = Vector3.one * 10;
	}

	void OnCollisionEnter(Collision coll)
	{
		if(coll.gameObject.tag == "Projectile" && invincibility <= 0)
		{
			Destroy(coll.gameObject);
			health --;
			invincibility = 30;
			print ("CENA DAMAGE");
			if(health <= 0)
				Application.LoadLevel("title");
		}
	}
}

public class ElementBossBeam : Element
{
	cena cen;
	
	float timer = 480;

	Vector3 projectile_aim_pos = Vector3.zero;


	public ElementBossBeam (cena cen)
	{
		this.cen = cen;
	}
	
	public override void onActive()
	{
		cen.ps.Play();
		projectile_aim_pos = floating_platform.instance.transform.position;
	}

	public override void onRemove()
	{
		Element.addElement(cen.ele_queue, new ElementStandardBoss(cen));
		cen.ps.Stop();
	}
	
	public override void update(float time_delta_fraction)
	{
		timer -= 1;
		if(timer <= 0)
		{
			finished = true;
		}

		if(timer <= 500)
		{
			GameObject new_projectile = MonoBehaviour.Instantiate(cen.cena_beam_prefab, cen.transform.position + cen.cena_beam_offset + Vector3.up * 25, Quaternion.identity) as GameObject;
			new_projectile.GetComponent<Rigidbody>().velocity = Vector3.forward * -100;
		}
	}
}

public class ElementStandardBoss : Element
{
	cena cen;

	float timer = 480;
	float timer2 = 600;
	
	public ElementStandardBoss (cena cen)
	{
		this.cen = cen;
	}
	
	public override void onActive()
	{
		cen.ps.Stop();
	}
	
	public override void update(float time_delta_fraction)
	{
		// Track players.
		Vector3 desired_position = new Vector3(floating_platform.instance.transform.position.x,
		                                       floating_platform.instance.transform.position.y,
		                                       cen.transform.position.z) + cen.chase_offset;

		cen.transform.position += (desired_position - cen.transform.position) * 0.01f;

		timer -= 1;
		timer2 -= 1;
		if(timer <= 0)
		{
			timer = 480;
			fire ();
		}

		if(timer2 <= 0)
		{
			Element.disruptElement(cen.ele_queue, new ElementBossBeam(cen));
		}
	}

	public void fire()
	{
		for(int i = 0; i < 1; i++)
		{
			Vector3 pos = floating_platform.instance.transform.position + Vector3.up * 6 + UnityEngine.Random.insideUnitSphere * 5;
			GameObject new_enemy = MonoBehaviour.Instantiate(cen.enemy_prefab, pos, Quaternion.identity) as GameObject;
		}
	}
}