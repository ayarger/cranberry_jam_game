﻿using UnityEngine;
using System.Collections;

public class rotate : MonoBehaviour {

	public float speed = 2f;
	public int axis = 2;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (axis == 0) {
			transform.Rotate (Time.deltaTime * speed, 0, 0);
		} else if (axis == 1) {
			transform.Rotate (0, Time.deltaTime * speed, 0);
			
		} else{
			transform.Rotate (0, 0, Time.deltaTime * speed);

		}
	}
}
