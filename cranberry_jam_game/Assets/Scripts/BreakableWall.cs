﻿using UnityEngine;
using System.Collections;

public class BreakableWall : MonoBehaviour {

    public bool damaged = false;
    public float health = 3f;
    private Vector3 homePosition;
    public float intensity = 0.1f;
    public GameObject anchor;
    public GameObject hitExplosion;

    // Use this for initialization
    void Start() {
		anchor = new GameObject();
		anchor.transform.position = transform.position;
        homePosition = anchor.transform.position;
        this.transform.position = anchor.transform.position;
    }


    void OnTriggerEnter(Collider other) {
        //Debug.Log("collindng");
        if (other.gameObject.tag == "Projectile") {
            health--;
            GameObject hit = Instantiate(hitExplosion);
            hit.transform.position = this.transform.position;
            hit.transform.rotation = this.transform.rotation;
            Destroy(other.gameObject);
        }
        damaged = true;
    }


    // Update is called once per frame
    void Update() {
        if (damaged) {
            this.transform.position = new Vector3(anchor.transform.position.x + (Random.value * intensity), anchor.transform.position.y + (Random.value * intensity), anchor.transform.position.z + (Random.value * intensity));
            //if (!this.GetComponent<AudioSource>().isPlaying) { 
            //    this.GetComponent<AudioSource>().Play();
            //}
            
        }
        else {
            this.transform.position = anchor.transform.position;
            //if (this.GetComponent<AudioSource>().isPlaying)
            //{
            //    this.GetComponent<AudioSource>().Stop();
            //}
        }
        this.transform.rotation = anchor.transform.rotation;

		print ("HEALTH: " + health.ToString());
        if (health <= 0f) {
            Destroy(this.gameObject);
        }
        damaged = false;
    }
}

