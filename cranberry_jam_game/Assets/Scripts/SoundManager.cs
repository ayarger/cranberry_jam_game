﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {
    public GameObject bark_obj;
    public static AudioClip bark;
    public GameObject punch_obj;
    public static AudioClip punch;
    public GameObject fall_obj;
    public static AudioClip fall;
    public GameObject wind_obj;
    public static AudioClip wind;
    public GameObject explosion_obj;
    public static AudioClip explosion;
    public GameObject confirmation_obj;
    public static AudioClip confirmation;
    public GameObject cannonFire_obj;
    public static AudioClip cannonFire;

    void Start () {
        bark = bark_obj.GetComponent<AudioClip>();
        punch = punch_obj.GetComponent<AudioClip>();
        fall = fall_obj.GetComponent<AudioClip>();
        wind = wind_obj.GetComponent<AudioClip>();
        explosion = explosion_obj.GetComponent<AudioClip>();
        confirmation = confirmation_obj.GetComponent<AudioClip>();
        cannonFire = cannonFire_obj.GetComponent<AudioClip>();
    }
	
	
	void Update () {
	
	}
}
