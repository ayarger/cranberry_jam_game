﻿using UnityEngine;
using System.Collections;

public class enemyVision : MonoBehaviour {
	public GameObject host;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider coll){
		if(coll.gameObject.tag == "Player"){
			if(host.GetComponent<enemyController> ().poi == null){
				host.GetComponent<enemyController> ().poi = coll.transform;
				host.GetComponent<enemyController> ().chargingUp = true;
				host.GetComponent<enemyController> ().detectTime = Time.time;
				host.GetComponent<enemyController> ().attacking = true;
				this.gameObject.GetComponent<SphereCollider>().enabled = false;
			}
		}
	}
}
