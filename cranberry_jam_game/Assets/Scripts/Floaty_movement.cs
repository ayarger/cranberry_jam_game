﻿using UnityEngine;
using System.Collections;

public class Floaty_movement : MonoBehaviour {
    public float angle = .2f;
    public float period = .1f;
    public float speed = .2f;
	public float amplitude = 1f;
	public float offsetX = 0f;
	public float offsetY = 0f;
	public float offsetZ = 0f;

    private float y0;
    private float time;

    // Update is called once per frame
    void Start() {
        y0 = transform.position.y;
    }
    void Update () {
        time = time + Time.deltaTime;
        float phase = Mathf.Sin(time / period);
        float phase2 = Mathf.Cos(time / period);
		transform.localRotation = Quaternion.Euler(new Vector3(phase * angle + offsetX, phase2 * angle + offsetY, phase * angle + offsetZ));

        this.GetComponent<Rigidbody>().AddForce(new Vector3(0, y0 + amplitude* Mathf.Sin(speed * time), 0));
    }
}
