﻿using UnityEngine;
using System.Collections;

public class CannonShoot : MonoBehaviour {
    bool cannon_one = true;
    public bool test_shoot = false;
    public float reload_time = .1f;
    public float timer = 0f;
    public float shoot_speed = 4f;
    public float num_players = 0f;
    public GameObject cannon_ball;
    public Transform cannon_location1;
    public Transform cannon_location2;

	public AudioClip cannon_shoot_clip;
 
    private bool reloading = false;
	
	void OnTriggerEnter (Collider other) {
        if (other.gameObject.tag == "Player") {
            num_players++;
            Debug.Log("ENTER");
        }
	}

    void OnTriggerExit(Collider other) {
        if (other.gameObject.tag == "Player") {
            num_players--;
            Debug.Log("Exit");
        }

    }





    void Update() {
        if (num_players != 0) {
            if (!reloading) {
                GameObject can_bal_inst = Instantiate(cannon_ball);
				AudioSource.PlayClipAtPoint(cannon_shoot_clip, Camera.main.transform.position);
                if (cannon_one) {
                    can_bal_inst.transform.position = cannon_location1.transform.position;
                    can_bal_inst.transform.rotation = cannon_location1.transform.rotation;
                }
                else {
                    can_bal_inst.transform.position = cannon_location2.transform.position;
                    can_bal_inst.transform.rotation = cannon_location2.transform.rotation;
                }
                can_bal_inst.GetComponent<Rigidbody>().velocity = can_bal_inst.transform.up * shoot_speed;
                reloading = true;
                cannon_one = !cannon_one;
            }
            else {
                if (timer < reload_time / num_players) {
                    timer += Time.deltaTime;
                }
                else {
                    timer = 0f;
                    reloading = false;
                }
            }
        }

		if(num_players <= 0)
			GetComponent<Renderer>().material.color = new Color(1, 0, 0);
		else
		{
			GetComponent<Renderer>().material.color = new Color(0, num_players * 0.2f, 0);
		}
    }
}