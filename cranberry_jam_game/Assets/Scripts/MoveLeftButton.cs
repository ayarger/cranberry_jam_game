﻿using UnityEngine;
using System.Collections;

public class MoveLeftButton : MonoBehaviour {

    public bool going_up = true;
    public int num_players = 0; //Number of players standing on the button
    public float velocity = 3f;
    public float max_height = 10f;
    public GameObject platform;
    private Rigidbody rb;

    void Start() {
        if (!going_up) {
            velocity = -velocity;
        }
        rb = platform.GetComponent<Rigidbody>();
    }

    void OnTriggerEnter(Collider other) {
        if(other.gameObject.tag == "Player") {
            if (num_players < 4) { //just in case
                num_players++;
            }

        }
    }

    void OnTriggerExit(Collider other) {
        if(other.gameObject.tag == "Player") {
            if (num_players > 0) {   //just in case
                num_players--;
            }
            Vector3 position = transform.position;
            position.y += .001f;
            transform.position = position;
        }
    }

	void Update () {
		floating_platform.instance.num_players_left = num_players;


		if(num_players <= 0)
			GetComponent<Renderer>().material.color = new Color(1, 0, 0);
		else
		{
			GetComponent<Renderer>().material.color = new Color(0, num_players * 0.2f, 0);
		}
	}
}
