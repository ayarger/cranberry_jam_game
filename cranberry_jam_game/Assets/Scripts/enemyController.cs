﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class enemyController : MonoBehaviour {
	public Transform poi;
	public float speed = 1f;
	public float rotationSpeed = 1f;
	public float knockBackForce = 5f;
	public float upForce = 1f;
	public bool chargingUp = false;
	public float detectTime = 0f;
	public float chargeTime = 0.5f;
	public float attackCooldown = 1f;
	public float attackCooldownTimer = 1f;
	public GameObject vision;
	public bool attacking = false;
	public float attackSpeedMult = 2f;

	public List<Element> ele_queue = new List<Element>();

	// Use this for initialization
	void Start () {
		Element.addElement(ele_queue, new ElementStandardEnemyMovement(this));
	}
	
	// Update is called once per frame
	void Update () {
		Element.updateElements(ele_queue);
	}

	void OnCollisionEnter(Collision coll){
		Element.collideElement(ele_queue, coll);
	}

	void OnTriggerEnter(Collider coll){
		Element.triggerElement(ele_queue, coll);
	}
}

class ElementStandardEnemyMovement : Element
{
	enemyController enemy;

	public Vector3 last_valid_position;

	public ElementStandardEnemyMovement(enemyController enemy)
	{
		this.enemy = enemy;
	}

	public override void update(float time_delta_fraction)
	{
		//Look at target
		//this.transform.LookAt (poi);
		if (enemy.poi != null) {
			enemy.transform.rotation = Quaternion.Slerp (enemy.transform.rotation, Quaternion.LookRotation (enemy.poi.transform.position - enemy.transform.position), enemy.rotationSpeed * Time.deltaTime);
		}
		//move forward
		if (!enemy.chargingUp) {
			//this.GetComponent<Rigidbody> ().AddForce (this.transform.forward*2f);
			Vector3 movement = new Vector3 (enemy.transform.forward.x, 0f, enemy.transform.forward.z);
			if(enemy.attacking)
				movement = movement*enemy.attackSpeedMult;
			enemy.transform.position += movement * enemy.speed * Time.deltaTime;
		}
		if (enemy.chargingUp) {
			if(Time.time>(enemy.detectTime+enemy.chargeTime))
				enemy.chargingUp = false;
		}
		
		if (!enemy.attacking) {
			if (enemy.attackCooldownTimer <= 0) {
				//Search for enemy
				enemy.vision.GetComponent<SphereCollider> ().enabled = true;
			} else{
				enemy.attackCooldownTimer = enemy.attackCooldownTimer - Time.deltaTime;
			}
		}

		// death
		if(enemy.transform.position.y - floating_platform.instance.transform.position.y < -5 || Mathf.Abs(enemy.transform.position.x - floating_platform.instance.transform.position.x) > 15 || Mathf.Abs(enemy.transform.position.z - floating_platform.instance.transform.position.z) > 15)
		{
			MonoBehaviour.Destroy(enemy.gameObject);
		}

		// Stay attached to ground.
		RaycastHit hit;
		if (Physics.Raycast(enemy.transform.position, -Vector3.up, out hit, 10)) {
			if(hit.collider.gameObject.tag == "player_platform")
			{
				last_valid_position = hit.point;
			}
		}
		if(last_valid_position != null)
		{
			enemy.transform.position = last_valid_position + Vector3.up * enemy.transform.lossyScale.y * 0.5f;
		}
	}

	public override void onTrigger(Collider coll)
	{
		if(coll.gameObject.tag == "punch_object")
		{
			MonoBehaviour.print("HIT");
			Vector3 trajectory = (enemy.transform.position - coll.gameObject.transform.position) * 5;
			trajectory += Vector3.up * 3;
			
			Element.disruptElement(enemy.ele_queue, new ElementDizzied(enemy.GetComponent<Entity>(), trajectory, 90));
		}
	}

	public override void onCollision(Collision coll)
	{
		if (coll.gameObject.tag == "Player") {
			Vector3 knockBackVector = (enemy.transform.forward * enemy.knockBackForce);
			knockBackVector.y = enemy.upForce;
			coll.gameObject.GetComponent<Rigidbody> ().AddForce (knockBackVector);
			//MonoBehaviour.print("KNOCKBACK");
			enemy.attackCooldownTimer = enemy.attackCooldown;
			//MonoBehaviour.print (enemy.transform.forward);
			enemy.poi = null;
			enemy.attacking = false;
		}
	}
}
