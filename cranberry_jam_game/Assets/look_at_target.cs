﻿using UnityEngine;
using System.Collections;

public class look_at_target : MonoBehaviour {

	public GameObject target;
	public float ease;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.forward = Vector3.Slerp(transform.forward, target.transform.position - transform.position, ease);
	}
}
