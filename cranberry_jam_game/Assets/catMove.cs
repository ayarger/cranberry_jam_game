﻿using UnityEngine;
using System.Collections;

public class catMove : MonoBehaviour {
	public Transform p1;
	public Transform p2;
	public bool active = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (active) {
			this.transform.position = new Vector3 ((this.transform.position.x + p1.position.x) / 2, (this.transform.position.y + p1.position.y) / 2, (this.transform.position.z + p1.position.z) / 2);
		} else {
			this.transform.position = new Vector3 ((this.transform.position.x + p2.position.x) / 2, (this.transform.position.y + p2.position.y) / 2, (this.transform.position.z + p2.position.z) / 2);
		}
	}
}
