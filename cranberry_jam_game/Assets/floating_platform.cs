﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class floating_platform : MonoBehaviour {

	public static floating_platform instance;

	public int num_players_up = 0;
	public int num_players_down = 0;
	public int num_players_left = 0;
	public int num_players_right = 0;

	public Vector3 forward_movement;

	public Text warning_text;
	public float warning_text_sincounter = 0.0f;
	bool warning = false;

	public AudioClip explosion_clip;

	public Rigidbody rb;

	public int health = 2;

	public List<Image> heart_images;

	public int invincibility = 30;

	public GameObject explosion_prefab;

	public GameObject hangar_lifting_ceiling;
	public GameObject hangar_enemy;
	public GameObject hangar_enemy_prefab;
	public bool began_hangar_fight = false;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody>();
	}

	void Awake()
	{
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		int horizontal_delta = num_players_right - num_players_left;
		int vertical_delta = num_players_up - num_players_down;
		rb.velocity = new Vector3(horizontal_delta * 0.1f + rb.velocity.x, vertical_delta * 0.1f + rb.velocity.y, 0) + forward_movement;

		if(Mathf.Abs(rb.velocity.x) > 10)
			rb.velocity = new Vector3(rb.velocity.x * 0.9f, rb.velocity.y, rb.velocity.z) + forward_movement;
		else
			rb.velocity = new Vector3(rb.velocity.x * 0.999f, rb.velocity.y, rb.velocity.z) + forward_movement;
		if(Mathf.Abs(rb.velocity.y) > 6)
			rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y * 0.9f, rb.velocity.z) + forward_movement;
		else
			rb.velocity = new Vector3(rb.velocity.x, rb.velocity.y * 0.999f, rb.velocity.z) + forward_movement;

		// make balloon swell / deflate
		Vector3 desired_scale = Vector3.one * (8 + vertical_delta);
		balloon.instance.transform.localScale += (desired_scale - balloon.instance.transform.localScale) * 0.1f;

		if(warning_text != null)
			check_for_obstacles();

		display_health();

		// hangar fight.
		if(began_hangar_fight)
		{
			if(hangar_enemy == null)
			{
				hangar_lifting_ceiling.transform.position += Vector3.up;
			}
		}
	}

	void check_for_obstacles()
	{
		Vector3 fwd = transform.TransformDirection(Vector3.forward);
		RaycastHit hit;
		Debug.DrawRay(transform.position, fwd * 125, Color.green);
		
		if (Physics.Raycast(transform.position, fwd, out hit, 125))
		{
			if (hit.collider.gameObject.tag == "platform_obstacle")
			{
				warning = true;
			}
			else
				warning = false;
		}
		else
		{
			warning = false;
		}

		if(warning)
		{
			warning_text.color = new Color(warning_text.color.r, warning_text.color.g, warning_text.color.b, Mathf.Sin(warning_text_sincounter));
			warning_text_sincounter += 0.1f;
		}
		else
		{
			warning_text.color = new Color(warning_text.color.r, warning_text.color.g, warning_text.color.b, 0);
		}
	}

	void display_health()
	{
		invincibility --;

		if(health == 0)
		{
			if(heart_images[0] != null)
				Destroy (heart_images[0]);
			if(heart_images[1] != null)
				Destroy (heart_images[1]);
			if(heart_images[2] != null)
				Destroy (heart_images[2]);
		}
		else if(health == 1)
		{
			if(heart_images[1] != null)
				Destroy (heart_images[1]);
			if(heart_images[2] != null)
				Destroy (heart_images[2]);
		}
		else if(health == 2)
		{
			if(heart_images[2] != null)
				Destroy (heart_images[2]);
		}
	}

	void OnCollisionEnter(Collision coll){
	
		if((coll.gameObject.tag == "platform_obstacle") && invincibility < 0)
		{
			print ("HURT");
			if(health > 1)
			{
				invincibility = 90;
				health --;
				AudioSource.PlayClipAtPoint(explosion_clip, Camera.main.transform.position);

				for(int i = 0; i < 25; i++)
				{
					GameObject new_explosion = Instantiate(explosion_prefab, transform.position + UnityEngine.Random.insideUnitSphere * 10, Quaternion.identity) as GameObject;
					new_explosion.transform.SetParent(transform);
				}
				Destroy (coll.gameObject);
			}
			else
			{
				Destroy(warning_text);
				camera_controller.instance.game_over ();
				Destroy (gameObject);
			}
		}
	}

	void OnTriggerEnter(Collider coll)
	{
		if(coll.gameObject.tag == "cutscene_trigger")
		{
			Application.LoadLevel("BossCutscene");
		}

		if(coll.gameObject.tag == "enemy_trigger")
		{
			coll.gameObject.GetComponent<enemy_trigger>().fire();
		}

		if(coll.gameObject.tag == "hangar_enemy_trigger")
		{
			began_hangar_fight = true;
			hangar_enemy = Instantiate(hangar_enemy_prefab, transform.position + Vector3.up * 5, Quaternion.identity) as GameObject;
			Destroy (coll.gameObject);
		}

		if((coll.gameObject.tag == "wall" || coll.gameObject.tag == "enemy_projectile") && invincibility < 0)
		{
			if(health > 1)
			{
				invincibility = 90;
				health --;
				AudioSource.PlayClipAtPoint(explosion_clip, Camera.main.transform.position);
				for(int i = 0; i < 25; i++)
				{
					GameObject new_explosion = Instantiate(explosion_prefab, transform.position + UnityEngine.Random.insideUnitSphere * 10, Quaternion.identity) as GameObject;
					new_explosion.transform.SetParent(transform);
				}
				Destroy (coll.gameObject);
			}
			else
			{
				Destroy(warning_text);
				camera_controller.instance.game_over ();
				Destroy (gameObject);
			}
		}
	}

	void OnTriggerStay(Collider coll)
	{
		if(coll.gameObject.tag == "cloud_ceiling")
		{
			rb.velocity = new Vector3(rb.velocity.x, -0.25f, rb.velocity.z);
		}
	}
}
